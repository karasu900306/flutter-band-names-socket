import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

enum ServiceStatus { Online, Offline, Connecting }

class SocketServices with ChangeNotifier {
  ServiceStatus _serviceStatus = ServiceStatus.Connecting;
  IO.Socket _socket;
  ServiceStatus get serverStatus => this._serviceStatus;
  IO.Socket get socket => this._socket;
  SocketServices() {
    this._initConfig();
  }
  void _initConfig() {
    this._socket = IO.io('https://66f8354da51c.ngrok.io', {
      'transports': ['websocket'],
      'autoConnect': true
    });
    this._socket.on('connect', (_) {
      this._serviceStatus = ServiceStatus.Online;
      notifyListeners();
    });
    this._socket.on('disconnect', (_) {
      this._serviceStatus = ServiceStatus.Offline;
      notifyListeners();
    });

    /*socket.on('nuevo-mensaje', (payload) {
      print('nuevo-mensaje: ');
      print('nombre: ' + payload['nombre']);
      print('mensaje: ' + payload['mensaje']);
      print(payload.containsKey('mensaje2')
          ? 'mensaje2: ' + payload['mensaje2']
          : 'No hay mensaje 2');
    });*/
  }
}
